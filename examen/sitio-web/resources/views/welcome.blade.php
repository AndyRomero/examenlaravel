<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">

        <title>Examen</title>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=drawing"></script>
    </head>
    <body>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="h1">Examen</p>
                <p>Búsquede de precios de gasolinas, según su ubicación</p>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6 mt-2">
                        <label for="estados">Estado:</label>
                            <select name="estados" id="estados" class="form-control">
                                <option value="0">-- Seleccione una Opcion --</option>
                                @foreach ($states as $element => $value)
                                    <option value="{{$value->estado}}">{{$value->estado}}</option>
                                @endforeach
                        </select>
                    </div>

                    <div class="col-md-6 mt-2">
                        <label for="municipios">Municipio:</label>
                        <select name="municipios" id="municipios" class="form-control"></select>
                    </div>

                    <div class="col-md-6 mt-2">
                        <label for="estado">Orden:</label>
                        <select name="orden" id="orden" class="form-control">
                            <option value="0">ASC</option>
                            <option value="1">DESC</option>
                        </select>
                    </div>

                    <div class="col-md-6 mt-4 pt-2">
                        <button class="btn btn-primary form-control" id="buscar">Buscar</button>
                    </div>
                </div>  
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div id="map" style="width: 450px; height: 400px;"></div>
                    </div>
                </div>
            </div>
        
        </div>

        <div class="row">
            <div class="col-md-12 mt-5">
                <div class="table-responsive text-center">
                    <table class="table table-striped table-bordered table-borderless table-hover table-sm" id="myTable">
                        <caption>Lista de gasolineras</caption>
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">_id</th>
                                <th scope="col">calle</th>
                                <th scope="col">rfc</th>
                                <th scope="col">razonsocial</th>
                                <th scope="col">date_insert</th>
                                <th scope="col">numeropermiso</th>
                                <th scope="col">fechaaplicacion</th>
                                <th scope="col">permisoid</th>
                                <th scope="col">longitude</th>
                                <th scope="col">latitude</th>
                                <th scope="col">codigopostal</th>
                                <th scope="col">colonia</th>
                                <th scope="col">municipio</th>
                                <th scope="col">estado</th>
                                <th scope="col">regular</th>
                                <th scope="col">premium</th>
                                <th scope="col">dieasel</th>
                            </tr>
                        </thead>
                        <tbody id="resultados"></tbody> 
                    </table>
                </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">

        /**
          * ejecutamos ajax para traer municipios segun el esad seleccionado
        **/

        $("#estados").change(function(event) {
            $(this)
            var estado = $("#estados option:selected").text();
            var settings = {
              "url": "/municipality/"+estado,
              "method": "GET",
              "timeout": 0,
            };

            $('#municipios option').remove();

            $.ajax(settings).done(function (response) {
                var municipios = JSON.parse(response).data;

                $.each(municipios, function (ind, elem) { 
                    var o = new Option(elem.municipio, elem.municipio);
                    /// jquerify the DOM object 'o' so we can use the html method
                    $(o).html(elem.municipio);
                    $("#municipios").append(o);
                }); 
            });
        });

        /**
          * ejecutamos ajax para traer resultados
        **/

        $("#buscar").click(function(event) {
            var estado = $("#estados option:selected").text();
            var municipio = $("#municipios option:selected").text();
            var orden = $("#orden option:selected").text();

            console.log(municipio)
            if(municipio === null){
                municipio = '0';
            }

            var settings = {
              "url": "/fuel-stations/"+estado+'/'+municipio,
              "method": "GET",
              "timeout": 0,
            };

            $.ajax(settings).done(function (response) {
                var data = JSON.parse(response).results;
                console.log(data)
                $("#resultados").html('')
                
                var locations = []
                var coordenadas = []

                $.each(data, function (ind, elem) { 
                    $("#resultados").append('<tr>'+
                        '<th scope="row">'+ elem._id + '</th>'+
                        '<td>'+ elem.calle + '</td>'+
                        '<td>'+ elem.rfc + '</td>'+
                        '<td>'+ elem.razonsocial + '</td>'+
                        '<td>'+ elem.date_insert + '</td>'+
                        '<td>'+ elem.numeropermiso + '</td>'+
                        '<td>'+ elem.fechaaplicacion + '</td>'+
                        '<td>'+ elem.permisoid + '</td>'+
                        '<td>'+ elem.longitude + '</td>'+
                        '<td>'+ elem.latitude + '</td>'+
                        '<td>'+ elem.codigopostal + '</td>'+
                        '<td>'+ elem.colonia + '</td>'+
                        '<td>'+ elem.municipio + '</td>'+
                        '<td>'+ elem.estado + '</td>'+
                        '<td>'+ elem.regular + '</td>'+
                        '<td>'+ elem.premium + '</td>'+
                        '<td>'+ elem.dieasel + '</td></tr>'
                    )
                    coordenadas = [elem.colonia,elem.latitude,elem.longitude]
                    locations.push(coordenadas)
                });
                table = $('#myTable').DataTable()
                table.destroy();
                table = $('#myTable').DataTable({"order": [ 0, orden ]});

               /*Mapa*/
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 4,
                    center: new google.maps.LatLng(19.4978,  -99.1269),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var infowindow = new google.maps.InfoWindow();

                var marker, i;

                for (i = 0; i < locations.length; i++) {  
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map
                    });

                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            infowindow.setContent(locations[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }
               /*fin mapa*/
            });//fin ajax
        });//fin click buscar

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: new google.maps.LatLng(19.4978,  -99.1269),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();
           
  </script>

  </body>
</html>