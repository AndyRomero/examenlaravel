<?php 

namespace App\Services;

use App\Traits\ConsumesExternalServices;

/**
 * 
 */
class ConsultaService{

	use ConsumesExternalServices;
	
	/**
 	 * The base uri to be used to consume the authors service
 	 * @var string	 
 	 **/
	public $baseUri;

	public function __construct(){
		$this->baseUri = config('services.api.base_uri');
	}

	/**
 	 * Get the full list of fuel station the api service
 	 * @var string	 
 	 **/
	public function obtainStates(){
		return $this->performRequest('GET','/states');
	}

	/**
 	 * Get the full list of fuel station the api service
 	 * @var string	 
 	 **/
	public function obtainMunicipality($estado){
		return $this->performRequest('GET','/municipality/'.$estado);
	}

	/**
 	 * Get the full list of fuel station the api service
 	 * @var string	 
 	 **/
	public function obtainFuelStation($estado = '0', $municipio = '0', $orden = 0){
		return $this->performRequest('GET','/fuel-stations-filters/'.$estado.'/'.$municipio.'/'.$orden);
	}

}