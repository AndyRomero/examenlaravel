<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\ConsultaService;
use Illuminate\Support\Facades\DB;

class BusquedaController extends Controller{

    /**
     * The service to consume the author service
     * @var AuthorService
     * @return void
     */
    public $consultaService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ConsultaService $consultaService){
        $this->consultaService = $consultaService;
    }

    /**
     * Return in view list fuel station
     * @return Illuminate\Http\Response
     */
    public function index(){
        $states = json_decode($this->states())->data;
        return view('welcome')->with([
                                        'states'  => $states,
                                    ]);
    }  

    /**
     * Return states list
     * @return Illuminate\Http\Response
     */
    public function states(){
        return $this->consultaService->obtainStates();
    }   

    /**
     * Return municipality list
     * @return Illuminate\Http\Response
     */
    public function municipality($estado){
        return $this->consultaService->obtainMunicipality($estado);
    }  

    /**
     * Return obtainFuelStation list
     * @return Illuminate\Http\Response
     */
    public function obtainFuelStation($estado = '0', $municipio = '0', $orden = 0){
        return $this->consultaService->obtainFuelStation($estado, $municipio, $orden);
    } 
}
