<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

$router->get('/', 'BusquedaController@index');
$router->get('/states', 'BusquedaController@states');
$router->get('/municipality/{state}', 'BusquedaController@municipality');
$router->get('/fuel-stations/{state?}/{municipality?}', 'BusquedaController@obtainFuelStation');