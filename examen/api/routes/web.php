<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * Users routes
 */
$router->get('/users', 'UserController@index');
$router->post('/users', 'UserController@store');
$router->get('/users/{user}', 'UserController@show');
$router->put('/users/{user}', 'UserController@update');
$router->patch('/users/{user}', 'UserController@update');
$router->delete('/users/{user}', 'UserController@destroy');

/**
 * Fuel stations
 **/

$router->get('/fuel-stations', 'FuelStationController@index');
$router->get('/fuel-stations-filters[/{estado}[/{municipio}[/{orden}]]]', 'FuelStationController@fuelStations');

/**
 * Ubication 
 */ 
$router->get('/postal-code', 'PostalCodeController@index');
$router->get('/states', 'PostalCodeController@states');
$router->get('/municipality/{state}', 'PostalCodeController@municipality');