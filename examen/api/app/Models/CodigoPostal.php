<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CodigoPostal extends Model {
    protected $table = "codigo_postal";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigo_postal', 
        'colonia',
        'asentamiento',
        'municipio',
        'estado',
        'ciudad',
        'zona',
    ];

}
