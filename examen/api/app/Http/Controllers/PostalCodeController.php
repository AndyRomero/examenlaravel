<?php

namespace App\Http\Controllers;

use App\Models\CodigoPostal;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class PostalCodeController extends Controller
{
    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return postalcode list
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        $postalcode = DB::table('codigo_postal')
                                ->select('estado','municipio', 'codigo_postal')
                                ->distinct()
                                ->get();

        return $this->successResponse($postalcode);
    }

    /**
     * Return estates list
     * @return Illuminate\Http\Response
     */
    public function states()
    {
        $postalcode =  DB::table('codigo_postal')
                                ->select('estado')
                                ->distinct()
                                ->get();

        return $this->successResponse($postalcode);
    }

    /**
     * Return estates list
     * @return Illuminate\Http\Response
     */
    public function municipality($state){
        $state = urldecode($state);
        $postalcode =  DB::table('codigo_postal')
                            ->select('municipio')
                            ->distinct()
                            ->where('estado', '=', $state)
                            ->get();

        return $this->successResponse($postalcode);
    }


   
}
