<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\FuelStation;
use Illuminate\Support\Facades\DB;


class FuelStationController extends Controller{
    use ApiResponser;

    /**
     * The service to consume the author service
     * @var AuthorService
     * @return void
     */
    public $fuelStationService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FuelStation $fuelStationService){
        $this->fuelStationService = $fuelStationService;
    }

    public function index(){
        return $this->fuelStationService->obtainFuelStation();
    }

    public function fuelStations($estado = '0', $municipio = '0', $orden = 0){
        $estado = urldecode($estado);
        $municipio = urldecode($municipio);

        /*Validamos si viene estado*/
        if($estado != '0'){
            $codigo_postal = DB::table('codigo_postal')
                                ->select('codigo_postal','estado','municipio','colonia')
                                ->distinct()
                                ->where('estado', '=', $estado)
                                ->get();
        }

        /*Validamos si viene municipio*/
        if($municipio != '0'){
            $codigo_postal = DB::table('codigo_postal')
                                ->select('codigo_postal','estado','municipio','colonia')
                                ->distinct()
                                ->where('municipio', '=', $municipio)
                                ->get();
        }



        $estaciones = json_decode($this->index());
        $estaciones = $estaciones->results;

        $datos = ["success" => true, "results" => array()];
        $contador = 0;
        if($estado != '0' or $municipio != '0'){
            foreach ($codigo_postal as $key => $value) {
                foreach ($estaciones as $llave => $valor) {
                    if($value->codigo_postal == $valor->codigopostal){
                        $aux[$llave] = $value->codigo_postal;
                        $data = [
                            "_id" => $valor->_id,
                            "calle" => $valor->calle,
                            "rfc" => $valor->rfc,
                            "razonsocial" => $valor->razonsocial,
                            "date_insert" => $valor->fechaaplicacion,
                            "numeropermiso" => $valor->numeropermiso,
                            "fechaaplicacion" => $valor->fechaaplicacion,
                            "permisoid" => $valor->﻿permisoid,
                            "longitude" => $valor->longitude,
                            "latitude" => $valor->latitude,
                            "codigopostal" => $valor->codigopostal,
                            "colonia" => $value->colonia,
                            "municipio" => $value->municipio,
                            "estado" => $value->estado,
                            "regular" => $valor->regular,
                            "premium" => $valor->premium,
                            "dieasel" => $valor->dieasel
                        ];
                        $datos['results'][$contador] = $data;
                        $contador++;
                    }
                }
            }

        }else{

            $codigo_postal = DB::table('codigo_postal')
                                ->select('codigo_postal','estado','municipio','colonia')
                                ->get();

            foreach ($estaciones as $key => $valor) {
                foreach ($codigo_postal as $llave => $value) {
                    if($value->codigo_postal == $valor->codigopostal){
                        $aux[$llave] = $value->codigo_postal;
                        $data = [
                            "_id" => $valor->_id,
                            "calle" => $valor->calle,
                            "rfc" => $valor->rfc,
                            "razonsocial" => $valor->razonsocial,
                            "date_insert" => $valor->fechaaplicacion,
                            "numeropermiso" => $valor->numeropermiso,
                            "fechaaplicacion" => $valor->fechaaplicacion,
                            "permisoid" => $valor->﻿permisoid,
                            "longitude" => $valor->longitude,
                            "latitude" => $valor->latitude,
                            "codigopostal" => $valor->codigopostal,
                            "colonia" => $value->colonia,
                            "municipio" => $value->municipio,
                            "estado" => $value->estado,
                            "regular" => $valor->regular,
                            "premium" => $valor->premium,
                            "dieasel" => $valor->dieasel
                        ];
                        $datos["results"][$contador] = $data;
                        $contador++;
                    }
                }
            }
        }

        /*Validamos el orden*/
        if($orden == 0){
            asort($datos);
        }else{
            arsort($datos);
        }

        return json_encode($datos);

    }
}
