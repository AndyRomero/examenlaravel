<?php 

namespace App\Services;

use App\Traits\ConsumesExternalServices;

/**
 * 
 */
class FuelStation{

	use ConsumesExternalServices;
	
	/**
 	 * The base uri to be used to consume the authors service
 	 * @var string	 
 	 **/
	public $baseUri;

	public function __construct(){
		$this->baseUri = config('services.estaciones.base_uri');
	}

	/**
 	 * Get the full list of fuel station the api service
 	 * @var string	 
 	 **/
	public function obtainFuelStation(){
		return $this->performRequest('GET','/v1//precio.gasolina.publico');
	}

}