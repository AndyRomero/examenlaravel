<?php

namespace Database\Seeders;

use App\Models\Author;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
       	DB::table('authors')->insert([
            'gender' => $gender = $faker->randomElement(['male','female']),
            'name' => $faker->name($gender),
            'country' => $faker->country,
        ]);
    }
}
